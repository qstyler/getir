import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PayloadDto } from './dto';

describe('AppController', () => {
  const getController = async (getRecords: jest.Mock<any, any>) => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: AppService,
          useValue: {
            getRecords: getRecords,
          },
        },
      ],
    }).compile();

    return app.get<AppController>(AppController);
  };

  it('should return records', async () => {
    const appController = await getController(
      jest.fn().mockResolvedValue([{}]),
    );

    const actual = await appController.getRecords(new PayloadDto());

    expect(actual).toMatchObject([{}]);
  });
});
