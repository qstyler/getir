import { IsDate, IsNotEmpty, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';

export class PayloadDto {
  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  startDate: Date;

  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  endDate: Date;

  @Type(() => Number)
  @IsNotEmpty()
  @IsNumber()
  minCount: number;

  @Type(() => Number)
  @IsNotEmpty()
  @IsNumber()
  maxCount: number;
}
