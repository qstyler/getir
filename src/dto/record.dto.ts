export class RecordDto {
  key: string;
  createdAt: Date;
  totalCount: number;
}
