import { RecordDto } from './record.dto';

export class ResponseDto {
  code: number;
  msg: string;
  records: RecordDto[];
}
