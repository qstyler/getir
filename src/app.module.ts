import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Record, RecordSchema } from './schemas';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGODB),
    MongooseModule.forFeature([{ name: Record.name, schema: RecordSchema }]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
