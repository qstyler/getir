import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Record, RecordDocument } from './schemas';
import { Model } from 'mongoose';
import { PayloadDto } from './dto';
import { RecordDto } from './dto/record.dto';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(Record.name) private recordModel: Model<RecordDocument>,
  ) {}

  async getRecords(payload: PayloadDto): Promise<RecordDto[]> {
    return this.recordModel
      .aggregate()
      .match({
        createdAt: {
          $gte: payload.startDate,
          $lt: payload.endDate,
        },
      })
      .group({
        _id: {
          key: '$key',
          createdAt: '$createdAt',
        },
        myCount: { $sum: 1 },
        totalCount: {
          $sum: {
            $sum: '$counts',
          },
        },
      })
      .match({
        totalCount: {
          $gte: payload.minCount,
          $lt: payload.maxCount,
        },
      })
      .project({
        _id: 0,
        key: '$_id.key',
        createdAt: '$_id.createdAt',
        totalCount: 1,
      });
  }
}
