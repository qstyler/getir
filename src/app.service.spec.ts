import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Record, RecordSchema } from './schemas';
import { MongoMemoryServer } from 'mongodb-memory-server';
import * as mongoose from 'mongoose';

describe('App Service Suite', () => {
  let appService: AppService;

  const mongod = new MongoMemoryServer();

  beforeAll(async () => {
    await mongod.start();
    const mongoUri = mongod.getUri();
    const module: TestingModule = await Test.createTestingModule({
      providers: [AppService],
      imports: [
        MongooseModule.forRootAsync({
          useFactory: async () => ({
            uri: mongoUri,
          }),
        }),
        MongooseModule.forFeature([
          {
            name: Record.name,
            schema: RecordSchema,
          },
        ]),
      ],
    }).compile();

    await mongoose.connect(mongoUri);

    await mongoose.connection.collection('records').insertMany([
      {
        key: '1',
        value: '1',
        createdAt: new Date('2022-01-28'),
        counts: [1, 2, 3],
      },
      {
        key: '1',
        value: '1',
        createdAt: new Date('2022-01-28'),
        counts: [4, 5, 6],
      },
      {
        key: '2',
        value: '2',
        createdAt: new Date('2022-01-30'),
        counts: [7, 8, 9],
      },
    ]);

    appService = module.get<AppService>(AppService);
  });

  it('should be defined', () => {
    expect(appService).toBeDefined();
  });

  it('should return collection of records', async () => {
    const records = await appService.getRecords({
      startDate: new Date('2022-01-27'),
      endDate: new Date('2022-01-29'),
      minCount: 1,
      maxCount: 30,
    });

    expect(records).toHaveLength(1);
    expect(records).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          totalCount: 21,
        }),
      ]),
    );
  });

  it('should return empty result filtered by date', async () => {
    const records = await appService.getRecords({
      startDate: new Date('2022-01-20'),
      endDate: new Date('2022-01-21'),
      minCount: 1,
      maxCount: 30,
    });

    expect(records).toHaveLength(0);
  });

  it('should return empty result filtered by total count', async () => {
    const records = await appService.getRecords({
      startDate: new Date('2022-01-27'),
      endDate: new Date('2022-01-30'),
      minCount: 30,
      maxCount: 50,
    });

    expect(records).toHaveLength(0);
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });
});
