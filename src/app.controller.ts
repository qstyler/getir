import { Controller, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { PayloadDto } from './dto';
import { RecordDto } from './dto/record.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  async getRecords(@Body() payload: PayloadDto): Promise<RecordDto[]> {
    return this.appService.getRecords(payload);
  }
}
